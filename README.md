# PWA with VueJS, Vuex, Firebase API

## Setup
### Clone this repository
```
git clone
```
### Install dependencies with [yarn](https://yarnpkg.com) package manager
```
yarn install
````
### Run development flow
```
yarn run serve
```
## To do
 * Add more node scripts
 * Setup enviroment variables
 * [Continuous integration](https://docs.gitlab.com/ce/ci/)